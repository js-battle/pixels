﻿document.addEventListener("DOMContentLoaded", Game);

function Game() {

	var word;
	var pressedLetters; // Массив с раннее введенными буквами
	var errors; // Кол-во ошибок
	var numberOpenLetters; // Кол-во открытых букв

	function StartNewGame()
	{
		$.ajax({url: "http://ragimov.info/js_battle/hangman_rus/"}).done(function( data ) 
		{
			word = data.name.toUpperCase();
			pressedLetters = [];
			errors = 0;
			numberOpenLetters = 0;

			document.getElementById('pressedLetters').innerHTML = '';
			document.getElementById('numberOfErrors').innerHTML = 'Oшибок 0 из 5 допустимых';
			document.getElementById('descriptionWord').innerHTML = '';	
			document.getElementById('squares').innerHTML = '';
			document.getElementsByClassName('img')[6].style.display = 'none';
			document.getElementsByClassName('img')[0].style.display = 'inline';
			document.getElementsByClassName("alf_main")[0].style.display = 'inline';
			document.getElementById('prav1').style.display = 'inline';
			document.getElementById('start').style.top = '40px';
			document.getElementById('start').style.left = '0px';
			document.getElementsByClassName('img3')[0].style.display = 'none';
			document.getElementsByClassName('img2')[0].style.display = 'none';
			document.getElementsByClassName('gif')[0].style.display = 'none';
			document.getElementsByClassName('img1')[0].style.display = 'inline';
			

			DrawingSquares(); // Генерация и позиционирование квадратиков под буквы
		});
	}

	function DrawingSquares()
	{
		var field = document.getElementById('squares');

		for (var i = 0; i < word.length; i++)
		{
			field.innerHTML += '<input type="text" name="input" class="display" maxlength="1" placeholder="" readonly>';
	
			var pixels = Math.round((screen.width - word.length * 60) / 2) + i * 50;
			document.getElementsByClassName("display")[i].style.left = pixels + "px";
		}
	}

	function VerificationLetterInWord(letter) 
	{

		if (numberOpenLetters == word.length || errors > 5)
			return;

		var errorChecking = true;

		for (var i = 0; i < pressedLetters.length; i++)
		{
			if (pressedLetters[i] == letter)
			{
				alert('Вы уже выбирали эту букву, попробуйте другую')
				return;
			}
		}

		for (var i = 0; i < word.length; i++)
		{
			if (word[i] == letter)
			{
				numberOpenLetters++;
				document.getElementsByClassName('display')[i].placeholder = letter;
				errorChecking = false;
			}
		}

		pressedLetters.push(letter);
		var outputPressedLetters = document.getElementById('pressedLetters');

		if (numberOpenLetters == word.length)
		{
			document.getElementsByClassName('img')[errors].style.display = 'none';
			document.getElementsByClassName('gif')[0].style.display = 'inline';
			document.getElementsByClassName('img1')[0].style.display = 'none';
			document.getElementsByClassName('img3')[0].style.display = 'inline';
		}
    
		if (errorChecking)
		{
			outputPressedLetters.innerHTML = outputPressedLetters.innerHTML + ' <strong class="red">' + letter + "</strong>";
			errors++;
			document.getElementById('numberOfErrors').innerHTML = 'Oшибок ' + errors + ' из 5 допустимых';
			document.getElementById('numberOfErrors').style.color = 'red';
			document.getElementsByClassName('img')[errors - 1].style.display = 'none';
			document.getElementsByClassName('img')[errors].style.display = 'inline';

			if (errors > 5)
			{
				document.getElementsByClassName('img1')[0].style.display = 'none';
				document.getElementsByClassName('img2')[0].style.display = 'inline';
				for (var i = 0; i < word.length; i++)
				{
					document.getElementsByClassName('display')[i].placeholder = word[i];
				}
			}
		}

		else 
			outputPressedLetters.innerHTML = outputPressedLetters.innerHTML + '  ' + letter;
	}

	document.getElementById('start').addEventListener("click", StartNewGame);

//Обработка событий клавиш интерфейса
	var letters = document.getElementsByClassName('letter');
	var clickButton = function(i) { return function ()
		{
			var letter = i == 6 ? String.fromCharCode(1025) : i > 6 ? String.fromCharCode(i + 1039) : String.fromCharCode(i + 1040);
			VerificationLetterInWord(letter);
		};
	}

	for (var i = 0; i < 33; i++)
	{
		letters[i].addEventListener("click", clickButton(i));
	}
}