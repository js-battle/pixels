﻿document.addEventListener("DOMContentLoaded", StartGame);

function StartGame()
{
	var dispensers = [];
	var whoseMove = 0;
	var lastLap = false;
	var wholeDeck = [];
	var deck = [];
	var resetCard = []; 
	var replacement = 0;
	var Players = [];

	var numberPlayers = +prompt('Введите кол-во искуственных интеллектов против вас(1-2)', 1);
	numberPlayers = numberPlayers > 1 ? 3 : 2;

	if (numberPlayers == 2) { 
		document.getElementsByClassName('cheap1')[0].style.display = 'none';
	}

	for (var i = 0; i < numberPlayers; i++) {
		Players[i] = {
			name: i ? 'Computer' + i : 'Human',
			cards: [],
			bank: 5,
			points : [],
			knockingFist: false
		};
	}

	for (var i = 0; i < Players.length; i++) {
		dispensers.push(i);
	}

	for (var i = 0; i < 52; i++) {
		wholeDeck[i] = {

			name: i % 13  == 0 ? 'A' : i % 13  == 1 ? 'two' : i % 13  == 2 ? 'three' : i % 13  == 3 ? 'four'
			: i % 13  == 4 ? 'five' : i % 13  == 5 ? 'six' : i % 13  == 6 ? 'seven' : i % 13  == 7 ? 'eight' : i % 13
				== 8 ? 'nine' : i % 13  == 9 ? 'ten' : i % 13  == 10 ? 'J' : i % 13  == 11 ? 'Q' : 'K',
			value: i  % 13 > 8 ? 10 : !(i % 13) ? 11 : (i + 1) % 13,
			lear: i < 13 ? 'diamonds' : i < 26 ? 'spades' : i < 39 ? 'clubs' : 'hearts',
			image: 'card' + (i + 1)
		};
	}

	// Хорошенько тосуем карты
	for (var i = 0; i < 10; i++) {
		wholeDeck.sort(compareRandom);
	}

	deck = wholeDeck.slice();

	while (dispensers.length != 1)
	{
		for (var i = 0; i < dispensers.length; i++){
			TakeCard(Players[dispensers[i]]);
		}

		dispensers = SearchMinValue(Players, dispensers);
	}
	
	var idDispenser = dispensers.pop();
	dispensers = null;

	for (var i = 0; i < Players.length; i++) {
		document.getElementById('bank' + Players[i].name).innerHTML = + Players[i].bank;
	}

	document.getElementById('startParty').style.display = 'inline';
	document.getElementById('informationBlock').innerHTML = 'The <strong>' + Players[idDispenser].name + 
		'</strong> is a distributor';

	whoseMove = idDispenser ? idDispenser - 1 : Players.length - 1;

	function StartParty()
	{
		document.getElementById('startParty').style.display = 'none';

		if (!Players[0].bank) {
			alert('Game over');
			return;
		}

		for (var i = 1; i < Players.length; i++) {

			if (!Players[i].bank) {

				alert('Player' + Players[i].name + 'left the game');
				document.getElementById(Players[i].name).innerHTML = '';
				document.getElementById('value' + Players[i].name).innerHTML = '';
				Players.splice(i, 1);
			}
		}
		
		if (Players.length == 1) {
			alert ('And the winner of the tournament to become - ' + Players[0].name);
			return;
		}

		ClearDesk();

		if (whoseMove % Players.length) {
			DistributingCards();
		}
		else {
			document.getElementById('distributingCards').style.display = 'inline';
		}
	}

	function DistributingCards()
	{
		document.getElementById('distributingCards').style.display = 'none';

		for (var i = 0; i < 3; i++) {

			for (var j = 0; j < Players.length; j++) {
				TakeCard(Players[j]);
			}
		}

		resetCard.push( deck.pop() );
		document.getElementById('resetCard').innerHTML = '<img class="card" src="images/' + 
			resetCard[resetCard.length - 1].image + '.png" >';
		
		for (var i = 0; i < Players.length; i++) {
			
			if ( CheckBlitz(Players[i].cards) ) {
				DecidingWinner();
				break;
			}
		}

		for (var i = 0; i < Players.length; i++) {
			UpdateDesk(Players[i]);
		}

		ContinuationGame();
	}

	function TakeCard(player)
	{
		document.getElementById('panel').style.display = 'none';
		
		if (!deck.length) {
			deck = resetCard;
			resetCard.push( deck.pop() );
			deck.reverse();
		}

		player.cards.push(deck.pop());
		player.points[player.cards[player.cards.length - 1].lear] += player.cards[player.cards.length - 1].value;

		document.getElementById(player.name).innerHTML += whoseMove ? '<img class="card" name = "' + 
			player.	cards[player.cards.length - 1].image + '" src="images/shirt.png" >' : '<img class="card" name = "'
				+ player.cards[player.cards.length - 1].image + '" src="images/' + player.cards
					[player.cards.length - 1].image + '.png" >';

		if (player.cards.length > 3 && player == Players[0]) {
			replacement = -1;
			document.getElementById('informationBlock').innerHTML = 'Select the card you want to lose';
		}
	}

	function ContinuationGame()
	{
		whoseMove = (whoseMove + 1) % Players.length;

		if (idDispenser == whoseMove && lastLap == true) {
			DecidingWinner();
		}

		else if (!whoseMove && !Players[whoseMove].knockingFist) {
			document.getElementById('panel').style.display = 'inline';
		}

		else if (whoseMove && !Players[whoseMove].knockingFist) {
			document.getElementById('informationBlock').innerHTML = '';
			DetermineMoveComputer(Players[whoseMove]);
		}

		else {
			lastLap = true;
			ContinuationGame();
		}
	}
	
	function ThrowOffCard(player, idCard)
	{
		resetCard.push( player.cards[idCard] );
		player.cards.splice(idCard, 1);

		UpdateDesk(player);
		ContinuationGame();
	}

	function SwapCards(player, idCard)
	{
		var temporaryVariable = resetCard.pop();

		resetCard.push( player.cards[idCard] );
		player.cards[idCard] = temporaryVariable;

		UpdateDesk(player);
		ContinuationGame();
	}

	function UpdateDesk(player, openCards)
	{
		document.getElementById(player.name).innerHTML = '';

		replacement = 0;
		player.points = FillingZeros(player.points);

		for (var i = 0; i < player.cards.length; i++) {
			document.getElementById(player.name).innerHTML += player.name == 'Human' || openCards ? 
				'<img class="card" name = "' + player.cards[i].image + '" src="images/' + player.cards[i].image + 
					'.png" >' : '<img class="card" name = "' + player.cards[i].image + '" src="images/shirt.png" >';

			player.points[player.cards[i].lear] += player.cards[i].value;
		}

		document.getElementById('resetCard').innerHTML = '<img class="card" name = "' + resetCard[resetCard.length - 1]
			.image + '" src="images/' + resetCard[resetCard.length - 1].image + '.png" >';
	}

	function CardReplacement()
	{
		document.getElementById('panel').style.display = 'none';
		document.getElementById('informationBlock').innerHTML = 'Select the card you want to replace';
		replacement = 1;
	}

	function FinishGame(player)
	{
		document.getElementById('panel').style.display = 'none';
		document.getElementById('informationBlock').innerHTML = '<strong>' + player.name + ' offered to finish the ' +
		'game</strong>';

		if (player != Players[0]) {
			alert(player.name + ' offered to finish the game')
		}

		player.knockingFist = true;
		UpdateDesk(player);
		ContinuationGame();
	}

	function DetermineMoveComputer(Computer) {

		var bestLear = DeterminingBestLear(Computer.points);
		var bestLearWithReset = DeterminingBestLear(Computer.points, resetCard[resetCard.length - 1]);
		
		Computer.cards.sort(SortPropertyValue);

		if (Computer.cards[0].lear == bestLearWithReset.lear && Computer.cards[0].value > 7) {
			var cardIdForSwap = Computer.cards[1].lear != bestLearWithReset.lear ? 1 : 
				Computer.cards[2].lear != bestLearWithReset.lear ? 2 : 0;
		}

		if (cardIdForSwap) {
			var card = Computer.cards[cardIdForSwap];
			Computer.cards[cardIdForSwap] = Computer.cards[0];
			Computer.cards[0] = card;
		}

		if ((resetCard[resetCard.length - 1].value >= Computer.cards[0].value && resetCard[resetCard.length - 1].lear
			== bestLearWithReset.lear) || (Computer.cards[0].lear != bestLearWithReset.lear && bestLearWithReset.value
				> 11)) {

			SwapCards(Computer, 0);
		}
		else if (bestLear.value > 27) {
			FinishGame(Computer);
		}
		else {
			TakeCard(Computer);
			bestLear = DeterminingBestLear(Computer.points);
			Computer.cards.sort(SortPropertyValue);

			var whetherReset = false;

			for (var i = 0; i < Computer.cards.length; i++) {

				if (Computer.cards[i].value < 8 || Computer.cards[i].lear != bestLear.lear) {
					ThrowOffCard(Computer, i);
					whetherReset = true;
					break;
				}
			}

			if (!whetherReset) {
				ThrowOffCard(Computer, 0);
			}
		}
	}

	function DecidingWinner() {

		var jackpot = 0;
		var bestLears = [];
		var winners = [];
		var WinnersAndLosers;
		var informationBlock = document.getElementById('informationBlock');

		informationBlock.innerHTML = '';

		for (var i = 0; i < Players.length; i++) {
			UpdateDesk(Players[i], true);
		}
		
		for (var i = 0; i < Players.length; i++) {
			Players[i].cards.sort(SortPropertyValue);

			var bestLear = DeterminingBestLear(Players[i].points);
			bestLears.push(bestLear.value);

			document.getElementById('value' + Players[i].name).innerHTML =  bestLear.lear + 
			', ' + bestLear.value;
		}

		WinnersAndLosers = SearchMinMaxPoints(bestLears);

		if (WinnersAndLosers.losePlayers.length == 1) {
			Players[WinnersAndLosers.losePlayers[0]].bank--;
			jackpot++;

			informationBlock.innerHTML = 'Lose - ' + Players[WinnersAndLosers.losePlayers[0]].name + 
				'<strong>  -1 chip</strong><br>';
		}
		else {

			for (var i = 0; i < Players.length; i++) {
				Players[i].bank--;
				jackpot++;
			}

			for (var i = 0; i < WinnersAndLosers.losePlayers.length; i++) {
				informationBlock.innerHTML += 'Lose - ' + Players[WinnersAndLosers.losePlayers[i]].name + 
				'<strong>  -1 chip</strong><br>';
			}
		}


		for (var i = 0; i < WinnersAndLosers.bestPlayers.length; i++) {
			winners.push( Players[WinnersAndLosers.bestPlayers[i]] );
		}

		winners.sort(SortByTopCard);

		for (var i = 0; i < winners.length; i++) {

			var chips = i ? jackpot / winners.length ^0 : jackpot / winners.length ^0 + jackpot % winners.length;
			winners[i].bank += chips; 
			informationBlock.innerHTML += chips > 1 ? '<br>Win - ' + winners[i].name + '  <strong>+' + (chips - 1) +
				' chips</strong><br>' : 'Win - ' + winners[i].name + '  <strong>+' + chips + ' chip</strong><br>';
		}

		for (var i = 0; i < Players.length; i++) {
			document.getElementById('bank' + Players[i].name).innerHTML = + Players[i].bank;
		}

		document.getElementById('startParty').style.display = 'inline';

		idDispenser = (idDispenser + 1) % Players.length;
		document.getElementById('informationBlock').innerHTML += 'The <strong>' + Players[idDispenser].name + 
			'</strong> is a distributor';
	}

	function ClearDesk()
	{
		for (var i = 0; i < Players.length; i++) {

			Players[i].cards = [];
			Players[i].knockingFist = false;
			Players[i].points = FillingZeros(Players[i].points);
			document.getElementById(Players[i].name).innerHTML = '';
			document.getElementById('value' + Players[i].name).innerHTML = '';
		}

		for (var i = 0; i < 10; i++) {
			wholeDeck.sort(compareRandom);
		}
		
		lastLap = false;
		resetCard = [];
		deck = wholeDeck.slice();
	}

	document.getElementById('startParty').addEventListener('click', StartParty);
	document.getElementById('distributingCards').addEventListener('click', DistributingCards);
	document.getElementById('takeCard').addEventListener('click', function(){TakeCard(Players[0])});
	document.getElementById('replaceCard').addEventListener('click', function(){CardReplacement()});
	document.getElementById('finishGame').addEventListener('click', function(){FinishGame(Players[0])});

	document.onmousedown = function(e)
	{
		if (e.target.parentNode.id == 'Human' && replacement) {
			var i = 0;

			while (Players[0].cards[i].image != e.target.name) {
				i++;
			}

			if (~replacement) {
				SwapCards(Players[0], i);
			}
			else {
				ThrowOffCard(Players[0], i);
			}
		}
	}
}

function Card(name, value, lear, image)
{
	this.name = name;
	this.lear = lear;
	this.value = value;
	this.image = image;
}

function compareRandom(a, b) 
{
	return Math.random() - 0.5;
}

function SortPropertyValue(card1, card2)
{
	return card1.value - card2.value;
}

function DeterminingBestLear(Points, card)
{
	if (arguments.length == 2) {
		Points[card.lear] = !Points[card.lear] ? 0 : Points[card.lear];
		Points[card.lear] += card.value;
	}
	
	var lear;
	var maxPoints = 0;

	for (var key in Points) {

		if (Points[key] > maxPoints) {
			maxPoints = Points[key];
			lear = key;
		}
	}

	return obj = {
		'lear': lear,
		'value': maxPoints
	};
}

function FillingZeros(points)
{
	for (var key in points) {
		points[key] = 0;
	}

	return points;
}

function CheckBlitz(cards)
{
	for (var i = 0; i < cards.length; i++) {

		if (cards[i].lear != cards[0].lear || (cards[i].name != 'K' && cards[i].name != 'A' 
			&& cards[i].name != 'ten')) {

			return false;
		}
	}
	
	return true;
}

function SearchMinValue(Players, dispensers)
{
	var minValueId = [];
	var cadrId = Players[dispensers[0]].cards.length - 1;

	minValueId.push(dispensers[0]);

	for (var i = 1; i < dispensers.length; i++) {

		if (Players[dispensers[i]].cards[cadrId].value < Players[minValueId[0]].cards[cadrId].value) {
			minValueId = [];
			minValueId.push(i);
		}
		else if (Players[dispensers[i]].cards[cadrId].value == Players[minValueId[0]].cards[cadrId].value) {
			minValueId.push(i);
		}
	}

	return minValueId;
}

function SearchMinMaxPoints(points) {

	var maxPoints = [];
	var minPoints = [];

	maxPoints.push(0);
	minPoints.push(0);

	for (var i = 1; i < points.length; i++) {

		if (points[i] > points[maxPoints[0]]) {
			maxPoints = [];
			maxPoints.push(i);
		}
		else if (points[i] == points[maxPoints[0]]) {
			maxPoints.push(i);
		}

		if (points[i] < points[minPoints[0]]) {
			minPoints = [];
			minPoints.push(i);
		}
		else if (points[i] == points[minPoints[0]]) {
			minPoints.push(i);
		}
	}
	
	return obj = {
		'bestPlayers': maxPoints,
		'losePlayers': minPoints
	}
}

function SortByTopCard(player1, player2) {

	var numberCards = player1.cards.length - 1;

	while (numberCards >= 0 && player1.cards[numberCards].value == player2.cards[numberCards].value) {
		numberCards--;
	}

	if (~numberCards) {
		return player2.cards[numberCards].value - player1.cards[numberCards].value;
	}
}