﻿document.addEventListener("DOMContentLoaded", function() {

	var specialChar = '\\';
	var specialCharBBCode = '[';
	var allocationTimeParse = 20; //Сколько времени выделить работе парса в 100 / value (5%).

	var canvas = document.getElementById('palitra');
	canvas.width = 150;
	canvas.height = 250;

	var palitra = canvas.getContext('2d');
	var widthGradient = 32;
	var moveSlider = false;
	var H, S = 100, V = 100;
	var colorText = '=#ff00';
	var move = false; // Был ли клик по градиенту (используется для ползунка в палитре).
	
	var gradient = palitra.createLinearGradient(0, canvas.height, widthGradient, 0);
	var hue = [[255,0,0],[255,0,255],[0,0,255],[0,255,255],[0,255,0],[255,255,0],[255,0,0]];
	
	var color;
	for (var i = 0; i < 7; i++) {
		color = 'rgb(' + hue[i][0] + ',' + hue[i][1] + ',' + hue[i][2] + ')';
		gradient.addColorStop(i * 1 / 6, color);
	}
	
	palitra.fillStyle = gradient;
	palitra.fillRect(0, 0, widthGradient, canvas.height);

	var slider = document.getElementById('slider');

	slider.style.top = canvas.offsetTop - slider.height / 2 + 200 + 'px';
	slider.style.left = canvas.offsetLeft - widthGradient * 9 / 32 + 'px';

	function insertBBCodes(bbCode) {

		var obj = document.getElementsByClassName('rblock')[0];
		var text = obj.value;
		var firstIndex = obj.selectionStart;
		var lastIndex = obj.selectionEnd;

		if (firstIndex != lastIndex) {
			
			if (bbCode == 'color') {
				obj.value = text.slice(0, firstIndex) + '[' + bbCode + colorText + ']' +
					text.slice(firstIndex, lastIndex) + '[/' + bbCode + ']' + text.slice(lastIndex);
			}
			else {
				obj.value = text.slice(0, firstIndex) + '[' + bbCode + ']' + text.slice(firstIndex, lastIndex) +
					'[/' + bbCode + ']' + text.slice(lastIndex);
			}

			parseText();
		}
	}

	function setTimeParse() {

		var infoError = document.getElementById('infobox');
		var value = document.getElementById('setParse').value;

		if (value < 0.1) {
			infoError.innerHTML = 'Значение должно быть не менее 0.1';
			value = 0.1;
		}

		if (value > 50) {
			infoError.innerHTML = 'Значение не должно прeвышать 50';
			value = 50;
		}

		allocationTimeParse = 100 / value;
		document.getElementById('setParse').value = value;
	}

	function parseText() {

		var posLineBreak = 0;
		var posChar = 0;
		var posBB = 0;
		var pos_ = 0;
		var pos = 0;

		var text = document.getElementsByClassName('rblock')[0].value;

//line break
		for(;;) {
			var firstIndex = text.indexOf('\n', posLineBreak);

			if ( !~firstIndex ) {
				break;
			}
			text = text.slice(0, firstIndex) + '<br>' + text.slice(firstIndex + 1);

			posLineBreak = firstIndex + 1;
		}
//special char
		for(;;) {
			var firstIndex = text.indexOf(specialChar, posChar);

			if ( !~firstIndex ) {
				break;
			}

			posChar = firstIndex + 1;

			var lastIndex = parseSpecialChars(text, firstIndex + 1);
			var specialCode = specialChars[ text.slice(firstIndex + 1, lastIndex) ];

			

			if ( specialCode !== undefined ) {
				text = text[lastIndex] == '\\' ? text.slice(0, firstIndex) + specialCode + text.slice(lastIndex + 1) :
					text.slice(0, firstIndex) + specialCode + text.slice(lastIndex);
			}
		}
//bb codes
		for(;;) {

			var firstIndex = text.indexOf(specialCharBBCode, posBB);

			if ( !~firstIndex ) {
				break;
			}

			var lastIndex = parseBBCode(text, firstIndex + 1);
			var specialCode = BBCodes[ text.slice(firstIndex + 1, lastIndex) ];

			posBB = lastIndex;

			if ( specialCode !== undefined ) {
				text =  text.slice(0, firstIndex) + specialCode + text.slice(lastIndex + 1);
			}

			else if ( text.slice(firstIndex + 1, firstIndex + 6) == 'color') {
				text =  text.slice(0, firstIndex) + '<font color="' + text.slice(firstIndex + 7, lastIndex) +
					'">' + text.slice(lastIndex + 1);
			}

			else if ( text.slice(firstIndex + 1, firstIndex + 5) == 'text' ){
				text =  text.slice(0, firstIndex) + '<div align="' + text.slice(firstIndex + 6, lastIndex) +
					'">' + text.slice(lastIndex + 1);
			}
		}

//lower case
		for(;;) {

			var index = text.indexOf('_', pos_);
			
			if ( !~index || index == text.length - 2) {
				break;
			}

			var letter = index + 1 < text.length ? text[index + 1] : '';
			var nextText = index + 2 < text.length ? text.slice(index + 2) : '';
			
			if (letter == '\\') {
				text = text.slice(0, index + 1) + nextText;
			}
			else if (letter == '{') {
				var startIndex = index;

				while (text[index] && text[index] != '}') {
					index++;;
				}

				text = startIndex + 2 < index ? text.slice(0, startIndex) + '<sub><em>' + 
					text.slice(startIndex + 2, index) + '</em></sub>' + text.slice(index + 1) : text;
			}
			else if (letter != '_')
				text = text.slice(0, index) + '<sub>' + letter + '</sub>' + nextText;

			pos_ = index + 1;
		}
//uppercase
		for(;;) {

			var index = text.indexOf('^', pos);
			
			if ( !~index || index == text.length - 2) {
				break;
			}

			var letter = index + 1 < text.length ? text[index + 1] : '';
			var nextText = index + 2 < text.length ? text.slice(index + 2) : '';
			
			if (letter == '\\') {
				text = text.slice(0, index + 1) + nextText;
			}
			else if (letter == '{') {
				var startIndex = index;

				while (text[index] && text[index] != '}') {
					index++;;
				}

				text = startIndex + 2 < index ? text.slice(0, startIndex) + '<sup><em>' + 
					text.slice(startIndex + 2, index) + '</em></sup>' + text.slice(index + 1) : text;
			}
			else if (letter != '^')
				text = text.slice(0, index) + '<sup>' + letter + '</sup>' + nextText;

			pos = index + 1;
		}

		document.getElementsByClassName('lblock')[0].innerHTML = text;
	}


	function parseSpecialChars(text, index)
	{
		for ( var i = index; i < text.length; i++ ) {

			var charCode = text[i].charCodeAt(0);

			if ( charCode < 65 || charCode > 122 || (charCode > 90 && charCode < 97) )
				return i;

		}

		return i;
	}


	function parseBBCode(text, index)
	{

		while ( text.length - index && text[index] != ']' ) 
			index++;

		return index;
	}
	
	function timerDecoration(func) {
		var timer = 0;
		var timelastParse;
		var checkParse = false;
		var autoParse = false; 
		var infoTime = document.getElementById('infobox');

		return function() {

			if (autoParse) {
				autoParse = false;
				setTimeout(parseText, (timer * allocationTimeParse - performance.now() + timelastParse + 100) ); //100мс для учета погрешности.
				return;
			}

			if (checkParse) return;

			var start = performance.now();

			func.apply('this', arguments);
			timelastParse = performance.now();
			timer = timelastParse - start;

			checkParse = true;
			autoParse = true;

			infoTime.innerHTML = 'Частота обновления текста: ' + timer * allocationTimeParse + 'ms';
			setTimeout(function() { checkParse = false; }, timer * allocationTimeParse);
		}
	}
	
	function getCursorPosition(e, click) {

		var x = e.pageX;
		var y = e.pageY;

		var br = canvas.getBoundingClientRect();
		var x2 = x - br.left;
		var y2 = y - br.top;
		
		if (click) {

			if (x2 < 50 && e.target == canvas) {

				move = true
				slider.style.top = canvas.offsetTop - slider.height / 2 + y2 + 'px';
				H = y2 * 360 / canvas.height;
				getRGB();
			}
		}
		else {
			y2 = y2 > canvas.height ? canvas.height : y2 < 0 ? 0 : y2;

			slider.style.top = canvas.offsetTop - slider.height / 2 + y2 + 'px';
			H = y2 * 360 / canvas.height;
			getRGB();
		}
	}

	function getRGB() {

		var R, G, B;
		var Hi = H / 60 % 6^0;
		var Vmin = (100 - S) * V / 100;
		
		var a = (V - Vmin) * (H % 60) / 60;
		var Vinc = Vmin + a;
		var Vdec = V - a;
		
		switch(Hi) {
			case 0: R = V; G = Vinc; B = Vmin; break;
			case 1: R = Vdec; G = V; B = Vmin; break;
			case 2: R = Vmin; G = V; B = Vinc; break;
			case 3: R = Vmin; G = Vdec; B = V; break;
			case 4: R = Vinc; G = Vmin; B = V; break;
			case 5: R = V; G = Vmin; B = Vdec; break;
		}

		R = R * 255 / 100^0;
		G = G * 255 / 100^0;
		B = B * 255 / 100^0;

		palitra.fillStyle = 'rgb(' + R + ', ' + G + ', ' + B + ')';
		palitra.fillRect(80, 0, 70, 70);

		colorText = '=#' + R.toString(16) + G.toString(16) + B.toString(16);
	}
// События отечающие за парс текста.
	parseText = timerDecoration(parseText);
	document.getElementsByClassName('rblock')[0].addEventListener('keyup', parseText);
	document.getElementById('setParse').addEventListener('keyup', setTimeParse);
// Кнопки вставки тегов выделенного текста.
	document.getElementById('bold').addEventListener('click', function() { insertBBCodes('b') } );
	document.getElementById('itl').addEventListener('click', function() { insertBBCodes('i') } );
	document.getElementById('strk').addEventListener('click', function() { insertBBCodes('s') } );
	document.getElementById('undr').addEventListener('click', function() { insertBBCodes('u') } );
	document.getElementById('colorText').addEventListener('click', function() { insertBBCodes('color') } );
// События для палитры
	document.addEventListener('mousedown', function(e) { getCursorPosition(e, true) } );
	document.addEventListener('mousemove', function(e) { if (move) getCursorPosition(e); } );
	document.addEventListener('mouseup', function(e) { move = false; } );
	document.ondragstart = function() { return false }

	document.getElementById('setParse').onkeypress = function(e) {

		if (e.ctrlKey || e.altKey || e.metaKey) return;

		if (!e.which || !e.charCode || e.which < 32) return;
		var chr = String.fromCharCode(e.which);

		if (chr < '0' || chr > '9') {
			return false;
		}
	}
});