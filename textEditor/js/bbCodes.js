BBCodes = {
	'b': '<strong>',
	'/b': '</strong>',
	'i': '<em>',
	'/i': '</em>',
	'u': '<ins>',
	'/u': '</ins>',
	's': '<del>',
	'/s': '</del>',
	'/color': '</font>',
	'code': '<code>',
	'/code': '</code>',
	'/text': '</div>'
}