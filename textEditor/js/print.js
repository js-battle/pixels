document.addEventListener("DOMContentLoaded", function() {
	var elem = document.getElementById('prnt');
	elem.addEventListener('click', function() {
		var newWin = window.open("about:blank", "Print text", "width=300,height=300");
		var text = document.getElementsByClassName('lblock')[0].innerHTML;
		newWin.document.write(text);
		newWin.print();
	});
});