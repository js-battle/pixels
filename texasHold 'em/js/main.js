﻿
var Pixels = Pixels || {};

// Инициализация игроков
var numberHuman = 1; // only 1
var numberBots = 3;

var player;
var players = new Array();

for (var i = 0; i < numberHuman; i++) {
	player = new Player( i ? 'Human' + (i + 1) : 'Human');
	players.push(player);
}

for (var i = 0; i < numberBots; i++) {
	player = new Player(i ? 'Computer' + (i + 1) : 'Computer');
	players.push(player);
}

// Инициализация стола
Pixels.table = new GameTable(players, 10);
delete player;
delete players;
// Инициализация игры
Pixels.game = new Game(Pixels.table);

//Pixels.game.startGame();


document.getElementById('startParty').addEventListener('click', Pixels.game.startGame);
document.getElementById('distributingCards').addEventListener('click', Pixels.game.distributingCards);
document.getElementById('equalize').addEventListener('click', function(){Pixels.table.players[Pixels.game.whoWalks].equalize()});
document.getElementById('rateIncrease').addEventListener('click', function(){Pixels.table.players[Pixels.game.whoWalks].raiseBet()});
document.getElementById('foldCards').addEventListener('click', function(){Pixels.table.players[Pixels.game.whoWalks].foldCards()});

document.getElementById('setRaise').addEventListener('keyup', function() {
	var raise = +document.getElementById('setRaise').value;
	
	if (raise < Pixels.table.getBestBet() - Pixels.table.players[0].bet) {
		raise = Pixels.table.getBestBet() - Pixels.table.players[0].bet;
	}
	
	if (raise > Pixels.table.players[0].bankroll || isNaN(raise)) {
		raise = Pixels.table.players[0].bankroll;
	}
	
	document.getElementById('setRaise').value = raise;
});


function getRandomMinMax(min, max) { // Дря равномерного распределения вероятности каждого числа от min до max - 1.
	var rand = Math.random();

	return ~-rand ? min + (max - min) * rand ^ 0 : getRandomMinMax(min, max);
}