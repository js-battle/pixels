﻿GameTable = function(players, blind) {
	var blind = blind || 0;

	this.players = players || [];
	this.cards = [];
	this.pot = [0];
	this.deck = [];

	this.getBlind = function() {
		return blind;
	};
};

GameTable.prototype.removePlayer = function(id) {
	var name = this.players[id].getName();

	document.getElementById(name).remove();
	document.getElementById('bet' + name).remove();
	document.getElementById('bankroll' + name).remove();
	console.log(name + ' - банкрот, уходит из-за стола');

	this.players.splice(id, 1);
}

GameTable.prototype.addPlayer = function(player) {
	this.players.push(player);
}

GameTable.prototype.shuffleCards = function() {
	for (var i = 0; i < 10; i++) {
		this.deck.sort(compareRandom);
	}
};

GameTable.prototype.clearTable = function() {
	for (var i = 0; i < this.players.length; i++) {
		players[i].cards = [];
		players[i].fold = false;
		players[i].numPots = 0;
		players[i].details.bankrollUpGame = players[i].bankroll;

		console.log('За игровым столом: ' + this.players[i].getName());
		document.getElementById(this.players[i].getName()).innerHTML = '';
		document.getElementById(this.players[i].getName()).style.display = 'inline';
	}

	this.cards = [];
	this.deck = new Deck();

	document.getElementById('boardCards').innerHTML = '';
};

GameTable.prototype.updateTable = function() {
	for (var i = 0; i < this.players.length; i++) {
		document.getElementById('bet' + this.players[i].getName()).innerHTML = this.players[i].bet;
		document.getElementById('bankroll' + this.players[i].getName()).innerHTML = 'Bankroll: ' +
			this.players[i].bankroll;
	}
};

GameTable.prototype.getBestBet = function () {
	var bestBet = 0;

	for (var i = 0; i < this.players.length; i++) {
		bestBet = this.players[i].bet > bestBet ? this.players[i].bet : bestBet;
	}

	return bestBet;
};

GameTable.prototype.getWeakBet = function () {
	var weakBet = this.getBestBet();

	for (var i = 0; i < this.players.length; i++) {
		weakBet = this.players[i].bet < weakBet  && this.players[i].bet ? this.players[i].bet : weakBet;
	}

	return weakBet;
};

GameTable.prototype.distributionBet = function () {
	var table = Pixels.table;

	var weakBet = table.getWeakBet();
	var bestBet = table.getBestBet();

	if (weakBet == bestBet) {

		for (var i = 0; i < table.players.length; i++) {
			table.pot[table.pot.length - 1] = table.pot[table.pot.length - 1] != undefined ?table.pot[table.pot.length - 1] +
				table.players[i].bet : table.players[i].bet;
			table.players[i].bet = 0;
		}
		
		return;
	}

	else {
		for (var i = 0; i < table.players.length; i++) {
			table.pot[table.pot.length - 1] += weakBet;
			table.players[i].bet -= weakBet;

			if (!table.players[i].bet)
				table.players[i].numPots = table.pot.length;
		}

		table.pot.push(0);
		table.distributionBet();
	}
};

GameTable.prototype.reduceSizeCards = function (players) {

	for (var i = 0; i < Pixels.table.players.length; i++) {

		for (var j = 0; j < Pixels.table.players[i].cards.length; j++) {
			document.getElementById(Pixels.table.players[i].cards[j].image).className = 'cardMin';
		}
	}

	for (var i = 0; i < Pixels.table.cards.length; i++) {
		document.getElementById(Pixels.table.cards[i].image).className = 'cardMin';
	}

	for (var i = 0; i < players.length; i++) {

		for (var j = 0; j < players[i].bestCombination.cards.length; j++) {
			document.getElementById(players[i].bestCombination.cards[j].image).className = 'card';
		}
	}
};

Deck = function() {
	var fullDeck = [];

	for (var i = 0; i < 52; i++) {
		fullDeck[i] = {
			name: i % 13  == 0 ? 'A' : i % 13  == 1 ? 'two' : i % 13  == 2 ? 'three' : i % 13  == 3 ? 'four'
			: i % 13  == 4 ? 'five' : i % 13  == 5 ? 'six' : i % 13  == 6 ? 'seven' : i % 13  == 7 ? 'eight' : i % 13
				== 8 ? 'nine' : i % 13  == 9 ? 'ten' : i % 13  == 10 ? 'J' : i % 13  == 11 ? 'Q' : 'K',
			value: i % 13 ? (i % 13) + 1 : 14,
			lear: i < 13 ? 'diamonds' : i < 26 ? 'spades' : i < 39 ? 'clubs' : 'hearts',
			image: 'card' + (i + 1)
		};
	}
	
	return fullDeck;
};