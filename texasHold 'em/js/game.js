﻿Game = function(table) {
	this.table = table;
	this.dispenser = getRandomMinMax(0, table.players.length);
	this.whoWalks = this.dispenser;

	this.numberMove = 0;
}

Game.prototype.startGame = function() {
	var table = Pixels.table;

	table.clearTable();
	table.updateTable();
	table.shuffleCards();
// Определяются 2 игрока перед сдающим, и делаются ставки(до просмотра своих карт).
	var index = (table.players.length + Pixels.game.dispenser - 1) % table.players.length;

	table.players[index].placeBet(table.getBlind() / 2, false);
	table.players[Pixels.game.dispenser].placeBet(table.getBlind(), false);
	console.log(table.players[index].getName() + ' поставил пол-блайнда: ' + table.getBlind() / 2);
	console.log(table.players[Pixels.game.dispenser].getName() + ' поставил блайнд: ' + table.getBlind());

	Pixels.game.numberMove = 2;

	if (this.whoWalks % table.players.length) this.distributingCards();
	else document.getElementById('distributingCards').style.display = 'inline';

	document.getElementById('startParty').style.display = 'none';
}

Game.prototype.distributingCards = function() {
	

document.getElementById('distributingCards').style.display = 'none';

	for (var i = 0; i < 2; i++) {

		for (var j = 0; j < Pixels.table.players.length; j++) {
			Pixels.table.players[j].takeCard();
		}
	}

	Pixels.game.termsTrade();
}

Game.prototype.termsTrade = function() {
	var bestBet = this.table.getBestBet();

	this.whoWalks = (this.whoWalks + 1) % this.table.players.length;
	this.numberMove++;
	this.table.updateTable();

	if (this.numberMove > this.table.players.length) {
		var index = this.table.players.length - 1;

		while(index > -1 && (this.table.players[index].bet == bestBet || !this.table.players[index].bankroll ||
			this.table.players[index].fold) ) { 

			index--;
		}

		if (!~index) {
			console.log('Круг торговли закончен');
			console.log('');
			this.numberMove = 0;
			this.determineStageGame(); // Если у всех bet равняется максимальной ставки или банк пуст или чел пассанул, заканчиваем круг.
			return;
		}
	}

	if (this.table.players[this.whoWalks].getName().slice(0, 5) == 'Human' && !this.table.players[this.whoWalks].fold) {
		
		var callValue = this.table.getBestBet() - +this.table.players[this.whoWalks].bet > this.table.players[this
			.whoWalks].bankroll ? this.table.players[this.whoWalks].bankroll : this.table.getBestBet() - +this
				.table.players[this.whoWalks].bet;

		document.getElementById('panel').style.display = 'inline';
		document.getElementById('equalize').innerHTML = !callValue ? 'check' : 'call  ' + callValue;
		document.getElementById('setRaise').value = callValue;
	}

	else if (this.table.players[this.whoWalks].getName().slice(0, 8) == 'Computer' && !this.table.players[this.whoWalks]
		.fold) {
		this.table.players[this.whoWalks].determineMoveComputer();
	}

	else {
		this.termsTrade(); // Переход к следующему игроку.
	}
}

Game.prototype.determineStageGame = function() {
	// После круга забираем у всех bet и вкладываем в общий pot.
	Pixels.table.distributionBet();
	
	var outputPot = 'pot:' + Pixels.table.pot[0];
	for (var i = 1; i < Pixels.table.pot.length; i++) {
		outputPot += '<br>pot' + (i + 1) + ': ' + Pixels.table.pot[i];
	}
	
	document.getElementById('pot').innerHTML = outputPot;

	if (!this.table.cards.length) {

		//Раздача 3-ех карт на стол
		for (var i = 0; i < 3; i++) {
			this.table.cards.push( this.table.deck.pop() );

			document.getElementById('boardCards').innerHTML += '<img id="' + this.table.cards[i].image +
			'" class="card" src="images/' + this.table.cards[i].image + '.png" >';
		}

		
		this.termsTrade(); // Начало круга
	}

	else if (this.table.cards.length < 5) {
		this.table.cards.push( this.table.deck.pop() );

		document.getElementById('boardCards').innerHTML += '<img id="' + this.table.cards[this.table.cards.length - 1]
			.image + '" class="card" src="images/' + this.table.cards[this.table.cards.length - 1].image + '.png" >';


		this.termsTrade(); // Начало круга
	}

	else this.decidingWinner();
}

Game.prototype.decidingWinner = function() {
	
	for (var i = 0; i < Pixels.table.players.length; i++) {
		if (Pixels.table.players[i] == undefined) continue;
		document.getElementById(Pixels.table.players[i].getName()).innerHTML = '';
		
		for (var j = 0; j < 2; j++) {
			document.getElementById(Pixels.table.players[i].getName()).innerHTML += '<img id = "' + Pixels.table.
				players[i].cards[j].image + '" class="card" src="images/' + Pixels.table.players[i].cards[j].image
					+ '.png" >';
		}
	}
	// Считывание лучших комбинаций для каждого игрока и определения победителей.
	var repositoryPlayers = this.table.players.slice();;
	
	for (var i = 0; i < this.table.players.length; i++) {
		repositoryPlayers[i].id = i;
		repositoryPlayers[i].bestCombination = this.table.players[i].scanHand();

		if (!this.table.players[i].numPots) this.table.players[i].numPots = this.table.pot.length;

		if (this.table.players[i].fold) 
			repositoryPlayers[i].bestCombination.value = 15;
		else {
			var name = this.table.players[i].getName();
			var value = repositoryPlayers[i].bestCombination.name;

			document.getElementById('bet' + name).innerHTML = value;
		}
	}

	repositoryPlayers.sort(sortByCombination);

	Pixels.game.distributionChip(repositoryPlayers, 0);

	document.getElementById('panel').style.display = 'none';
	
	setTimeout( function() {

		for (var i = 0; i < Pixels.table.players.length; i++) {

			if (!Pixels.table.players[i].bankroll) {
				Pixels.table.removePlayer(i), i--;
			}
		}

		if (Pixels.table.players.length == 1) {
			console.log('С фишками отсался только: ' + Pixels.table.players[0].getName());
			return;
		}

		Pixels.table.pot = [0];
		Pixels.dispenser = (Pixels.game.dispenser + 1) % Pixels.table.players.length;
		Pixels.whoWalks = Pixels.game.dispenser;

		document.getElementById('startParty').style.display = 'inline';
	}, 3000);
}

Game.prototype.distributionChip = function(players, index) {
	var numWinners = [];

	var numberWinners = 1;
	while (numberWinners < players.length && players[0].bestCombination.value ==
		players[numberWinners].bestCombination.value) {
		numberWinners++;
	}

	for (var i = 0; i < numberWinners; i++) {
		
		for (var j = index; j < players[i].numPots; j++) {
			numWinners[j] = numWinners[j] != undefined ? numWinners[j] + 1 : 1;
		}
	}
	
	for (var i = 0; i < numberWinners; i++) {
		var jackpot = 0;
		
		for (var j = index; j < numWinners.length; j++) {
			jackpot += players[i].numPots > j ? this.table.pot[j] / numWinners[j] : 0;
		}

		console.log(this.table.players[players[i].id].getName() + ',  выигрыш составил: ' + (jackpot ^ 0)+ ', поставлено ' +
			'фишек в этой игре: ' + (this.table.players[players[i].id].details.bankrollUpGame - this.table.players
				[players[i].id].bankroll) );

		this.table.players[players[i].id].bankroll += jackpot ^ 0;
	}
	
	for (var j = index; j < numWinners.length; j++) 
		this.table.pot[j] = 0;

	if (players.length == this.table.players.length) 
		this.table.reduceSizeCards(players.slice(0, numberWinners));

	if (numWinners.length < this.table.pot.length) {
		this.distributionChip(players.slice(numberWinners), numWinners.length);
	}
	
	console.log('');
	console.log('');
}