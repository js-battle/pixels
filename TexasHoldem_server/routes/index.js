var path = require('path');
var url = require('url');
var qs = require('querystring');
var rooms = require('rooms');
var log = require('libs/log')(module);

module.exports = function(app) {
    app.get('/', function(req, res, next) {
        log.info('Пипол зашел на страничку с комнатами');
        // Подгружаем уже созданные комнаты
        res.render('index',{rooms: rooms.getRooms()});
        next();
    });

// Добавление комнаты(добавляется комната в список, + чувака сразу в нее кидает калбэком)
    app.get('/addRoom', function(req, res, next) {
        var options = getOptions(req.url);

        rooms.addRoom(res, options);

        res.end(rooms.getRooms().length - 1 + '');
    });

    app.get('/game', function(req, res, next) {

        res.render('holdem');
        next();
    });

    app.get('/identification', function(req, res, next) {

        if (req.headers.referer == undefined) {
            res.end('')
            return;
        }

        var idRoom = getIdRoom(req.headers.referer);
        log.info('Чел зашел в комнату с id: ' + idRoom);
        rooms.addPlayer(res, idRoom);
        next();
    });

    app.get('/removePlayer', function(req, res, next) {
        var options = getOptions(req.url);

        rooms.removePlayer(res, options);
        next();
    });


    app.get('/subscribe', function(req, res, next) {
        // Подписка со страницы комнат
        var options = getOptions(req.url);

        log.info('refer: ' + req.headers.referer);
        if (options.listRooms) {
            log.info('добавляем визитора');
            rooms.addVisitor(res);
            return;
        }

        log.info("поиск идентификатора: " + options.identificator);
        rooms.subscribe(res, options.identificator);
    });

    app.get('/dropCard', function(req, res, next) {
        var options = getOptions(req.url);

        rooms.dropCard(res, options);
        next();
    });

    app.get('/equalize', function(req, res, next) {
        var options = getOptions(req.url);

        rooms.eventOccurred(res, options, 'equalize');
    });

    app.get('/raiseBet', function(req, res, next) {
        var options = getOptions(req.url);

        rooms.eventOccurred(res, options, 'raise');
    });

    app.get('/fold', function(req, res, next) {
        var options = getOptions(req.url);

        rooms.eventOccurred(res, options, 'fold');
    });

    app.get('/decidingWinner', function(req, res, next) {
        var options = getOptions(req.url);

        rooms.decidingWinner(res, options);
    });

    function getOptions(link) {
        var query = url.parse(link).query;

        return qs.parse(query);
    }

    function getIdRoom(referer) {
        return referer.slice(referer.indexOf('=') + 1);
    }

}