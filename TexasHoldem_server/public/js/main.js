﻿var Pixels = Pixels || {};

function initializationGame(options) {
    var player;
    var players = new Array();

    for (var i = 0; i < options.maxPlayer; i++) {
        player = new Player( i ? 'Player' + (i + 1) : 'Player');//, bankroll);
        players.push(player);
    }

// Инициализация стола
    Pixels.table = new GameTable(players, 10);
    Pixels.table.playerHuman = options.playerIndex;
// Инициализация игры
    Pixels.game = new Game(Pixels.table);

    Pixels.playerIndex = options.playerIndex;
    Pixels.identificator = options.identificator;

    document.getElementById('distributingCards').addEventListener('click', Pixels.game.distributingCards);
    document.getElementById('equalize').addEventListener('click', equalize);
    document.getElementById('rateIncrease').addEventListener('click', raiseBet);
    document.getElementById('foldCards').addEventListener('click', foldCards);
    
    document.getElementById('setRaise').addEventListener('keyup', function() {
	var raise = +document.getElementById('setRaise').value;
	
	if (raise < Pixels.table.getBestBet() - Pixels.table.players[0].bet) {
		raise = Pixels.table.getBestBet() - Pixels.table.players[0].bet;
	}
	
	if (raise > Pixels.table.players[0].bankroll || isNaN(raise)) {
		raise = Pixels.table.players[0].bankroll;
	}
	
	document.getElementById('setRaise').value = raise;
});

    if (!Pixels.playerIndex) {
        document.getElementById('distributingCards').style.display = 'inline';
    }
}

function equalize() {
    /*var options;

     for (var i = 1; Pixels.game.whoWalks + i % Pixels.table.players.length >= Pixels.table.playerHuman; i++) {
     options.push(Pixels.table.players[Pixels.game.whoWalks + i].determineMoveComputer());
     }*/
    document.getElementById('panel').style.display = 'none';

    var result = checkFinishRound('equalize');

    if (result) {
        sendRequest(result.name, result.options);
    }
    else {
        // тогда просто уравниваем.
        sendRequest('equalize');
        Pixels.table.players[Pixels.game.whoWalks].equalize();
    }
}
// Нид сделать ограничение на ввод для raise(от value call до bankroll);
function raiseBet() {
    document.getElementById('panel').style.display = 'none';

    var newBet = +document.getElementById('setRaise').value;
    var result =  checkAbilityContinueParty('raise', newBet);

    if (result) {
        sendRequest(result.name, result.options);

    }
    else {
        sendRequest('raiseBet', {newBet: newBet});
        Pixels.table.players[Pixels.playerIndex].placeBet(newBet);
    }
}

function foldCards() {
    document.getElementById('panel').style.display = 'none';

    sendRequest('fold');
    Pixels.table.players[Pixels.playerIndex].foldCards();
}

function sendRequest(urlRequest, options) {
    var parameters = '';
    var options = options || {};

    for (key in options) {
        parameters += '&' + key + '=' + options[key];
    }

    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/' + urlRequest + '?identificator=' + Pixels.identificator + parameters, true);
    xhr.send();
}

function checkAbilityContinueParty(nameEvent, value) {

    var players = Pixels.table.players;
    var bestBet = Pixels.table.getBestBet();
    var numWithBankroll = bestBet - players[Pixels.playerIndex].bet >= value ? -1 : 0;

    for (var i = 0; i < players.length; i++) {
        if (players[i].bankroll && !players[i].fold) {
            numWithBankroll++;
        }
    }
    // Если с фишками в bankroll в середине игры осталось 0-1 людей, то смысла торговаться нет.
    if (numWithBankroll < 2) {
        return {name: 'decidingWinner', options: {noChips: true, nameEvent: nameEvent}};
    }

    return false;
}

function checkFinishRound(eventName) {

    var players = Pixels.table.players;
    var bestBet = Pixels.table.getBestBet();
    var numThrownOffAndBankrupts = 0;

    // Проверка на игроков впереди(которые сбросили или без bankroll), дабы не дать им управление
    while (players[(Pixels.game.whoWalks + numThrownOffAndBankrupts + 1) % players.length].fold
        || !players[(Pixels.game.whoWalks + numThrownOffAndBankrupts + 1) % players.length]
        .bankroll) {

        numThrownOffAndBankrupts++;
    }

    if (Pixels.game.numberMove + numThrownOffAndBankrupts + 1 > players.length &&
        Pixels.table.cards.length <= 5) {

        var index = players.length - 1;

        while( index > -1 && (players[index].bet == bestBet || !players[index].bankroll ||
            players[index].fold || index == Pixels.game.whoWalks)) {
            index--;
        }

        // Проверка на смысл торгов среди игроков.
        var result = checkAbilityContinueParty(eventName, players[Pixels.game.whoWalks].bankroll);
        if (result && !~index) {
            return result;
        }

        if (!~index) {
            if (Pixels.table.cards.length < 5) {
                return {name: 'dropCard'};
            }
            else {
                return {name: 'decidingWinner'};
            }
        }
    }

    return false;
}