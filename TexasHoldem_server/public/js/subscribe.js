identification();
console.log('ideitification start');

function identification() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/identification', true);

    xhr.onload = function() {
        var data = JSON.parse(this.responseText);
        console.log('You key: ' + data.identificator);
        console.log('You index: ' + data.playerIndex);

        initializationGame(data);
        subscribe();
    }

    xhr.onerror = function() {
        console.log('pichalka');
    }

    xhr.send();
}

function subscribe() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/subscribe?identificator=' + Pixels.identificator, true);

    xhr.onreadystatechange = function() {
        if (this.readyState != 4) return;
        if (this.status != 200) {
            setTimeout(subscribe, 100);
            return;
        }

        var info = JSON.parse(this.responseText);
        console.log(this.responseText);

        if (info.cards != undefined) {

            if (info.comment == 'no chips') {
                cardsForNoChips(info.cards, info.nameEvent);
            }

            else if (info.cards.length % 2) {
                Pixels.table.players[Pixels.game.whoWalks].equalize();
                cardsForTable(info.cards);
            }
            else if (info.comment == 'firstCards') {
                firstCards(info.cards);
            }
            else {
                cardsAllPlayers(info.cards);
                Pixels.table.players[Pixels.game.whoWalks].equalize();
            }
        }

        else if (info.eventName != undefined) {
            eventOccurred(info);
        }

        subscribe();
    }

    xhr.send();
}

function searchIndex(cards, card) {
    for (var i = 0; i < cards.length; i++) {
        if (cards[i].image == card.image) {
            return i;
        }
    }

    return -1;
}

function cardsForTable(cards) {

    for (var i = 0; i < cards.length; i++) {
        Pixels.table.cards.push(cards[i]);

        document.getElementById(Pixels.table.getName()).innerHTML += '<img id = "' + cards[i].image +
            '" class="card" src="images/' + cards[i].image + '.png" >';
    }
}

function cardsAllPlayers(cards) {

    for (var i = 0; i < Pixels.table.players.length; i++) {
        Pixels.table.players[i].cards = cards.slice(2 * i, 2 * (i + 1));

        document.getElementById(Pixels.table.players[i].getName()).innerHTML = '';

        for (var j = 0; j < 2; j++) {
            document.getElementById(Pixels.table.players[i].getName()).innerHTML += '<img id = "' + Pixels.table
                    .players[i].cards[j].image + '" class="card" src="images/' + Pixels.table.players[i].cards[j]
                        .image + '.png" >';
        }
    }
}

function firstCards(cards) {
    Pixels.game.startGame();
// Если вы не в режиме зрителя то получаете карты.
    if (~Pixels.playerIndex) {
        Pixels.table.players[Pixels.playerIndex].cards = cards;
    }

    for (var i = 1; i < cards.length / 2; i++) {
        Pixels.table.players[Pixels.table.playerHuman + i].cards = cards.slice(i * 2, (i + 1) * 2);
    }


    for (var i = 0; i < Pixels.table.players.length; i++) {
        for (var j = 0; j < 2; j++) {
            if (i == Pixels.playerIndex) {
                document.getElementById(Pixels.table.players[Pixels.playerIndex].getName()).innerHTML +=
                    '<img id = "' + Pixels.table.players[Pixels.playerIndex].cards[j].image + '" class="card" ' +
                        'src="images/' + Pixels.table.players[Pixels.playerIndex].cards[j].image + '.png" >';
            }
            else {
                document.getElementById(Pixels.table.players[i].getName()).innerHTML +=
                        '<img class="card" src="images/shirt.png">';
            }
        }
    }

    Pixels.game.termsTrade();
}

function eventOccurred(data) {

    switch (data.eventName) {
        case 'equalize' :
            Pixels.table.players[Pixels.game.whoWalks].equalize();
            break;

        case 'raise':
            Pixels.table.players[Pixels.game.whoWalks].placeBet(+data.newBet);
            break;

        case 'fold' :
            Pixels.table.players[Pixels.game.whoWalks].foldCards();
            break;

        case 'addPlayer' :
            Pixels.table.playerHuman++;
            break;
    }
}

function cardsForNoChips(cards, nameEvent) {
    console.log('last act: ' + nameEvent);

    eventOccurred({eventName: nameEvent, newBet: Pixels.table.getBestBet() -
        Pixels.table.players[Pixels.game.whoWalks].bet});

    Pixels.table.cards = [];
    document.getElementById(Pixels.table.getName()).innerHTML = '';

    cardsForTable(cards.slice(Pixels.table.players.length * 2));
    cardsAllPlayers(cards.slice(0, Pixels.table.players.length * 2));

    Pixels.game.decidingWinner();
}