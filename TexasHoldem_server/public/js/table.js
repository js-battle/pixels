﻿GameTable = function(players, blind) {
    var name = 'boardCards';
	var blind = blind || 10;

	this.players = players || [];
	this.cards = [];
	this.pot = [0];

	this.getBlind = function() {
		return blind;
	};

    this.getName = function() {
        return name;
    }
};

GameTable.prototype.removePlayer = function(id) {
	var name = this.players[id].getName();

	document.getElementById(name).remove();
	document.getElementById('bet' + name).remove();
	document.getElementById('bankroll' + name).remove();
	console.log(name + ' - банкрот, уходит из-за стола');

	this.players.splice(id, 1);
    Pixels.table.playerHuman--;
};

GameTable.prototype.addPlayer = function(player) {
	this.players.push(player);
}

GameTable.prototype.clearTable = function() {
	for (var i = 0; i < this.players.length; i++) {
		this.players[i].cards = [];
        this.players[i].fold = false;
        this.players[i].numPots = 0;
        this.players[i].details.bankrollUpGame = this.players[i].bankroll;

		console.log('За игровым столом: ' + this.players[i].getName());
		document.getElementById(this.players[i].getName()).innerHTML = '';
		document.getElementById(this.players[i].getName()).style.display = 'inline';
	}

	this.cards = [];

	document.getElementById('boardCards').innerHTML = '';
};

// Если указать в параметрах false, то не будет обновлять те или иные поля(это используется в коде)
GameTable.prototype.updateTable = function() {
	for (var i = 0; i < this.players.length; i++) {
        if (arguments[0] || arguments[0] == undefined)
		    document.getElementById('bet' + this.players[i].getName()).innerHTML = this.players[i].bet;

        if (arguments[1] || arguments[1] == undefined)
            document.getElementById('bankroll' + this.players[i].getName()).innerHTML = 'Bankroll: ' +
	            this.players[i].bankroll;
	}

    var outputPot = 'pot:' + Pixels.table.pot[0];
    for (var i = 1; i < Pixels.table.pot.length; i++) {
        outputPot += '<br>pot' + (i + 1) + ': ' + Pixels.table.pot[i];
    }

    document.getElementById('pot').innerHTML = outputPot;
};

GameTable.prototype.getBestBet = function () {
	var bestBet = 0;

	for (var i = 0; i < this.players.length; i++) {
		bestBet = this.players[i].bet > bestBet ? this.players[i].bet : bestBet;
	}

	return bestBet;
};

GameTable.prototype.getWeakBet = function () {
	var weakBet = this.getBestBet();

	for (var i = 0; i < this.players.length; i++) {
		weakBet = this.players[i].bet < weakBet  && this.players[i].bet ? this.players[i].bet : weakBet;
	}

	return weakBet;
};

GameTable.prototype.distributionBet = function () {
	var table = Pixels.table;

	var weakBet = table.getWeakBet();
	var bestBet = table.getBestBet();

	if (weakBet == bestBet) {

		for (var i = 0; i < table.players.length; i++) {
			table.pot[table.pot.length - 1] = table.pot[table.pot.length - 1] != undefined ?table.pot[table.pot.length - 1] +
				table.players[i].bet : table.players[i].bet;
			table.players[i].bet = 0;
		}
		
		return;
	}

	else {
		for (var i = 0; i < table.players.length; i++) {
            if (!table.players[i].bet) continue;
			table.pot[table.pot.length - 1] += weakBet;
			table.players[i].bet -= weakBet;

			if (!table.players[i].bet)
				table.players[i].numPots = table.pot.length;
		}

		table.pot.push(0);
		table.distributionBet();
	}
};

GameTable.prototype.reduceSizeCards = function (players) {

	for (var i = 0; i < Pixels.table.players.length; i++) {

		for (var j = 0; j < Pixels.table.players[i].cards.length; j++) {
			document.getElementById(Pixels.table.players[i].cards[j].image).className = 'cardMin';
		}
	}

	for (var i = 0; i < Pixels.table.cards.length; i++) {
		document.getElementById(Pixels.table.cards[i].image).className = 'cardMin';
	}

	for (var i = 0; i < players.length; i++) {

		for (var j = 0; j < players[i].bestCombination.cards.length; j++) {
			document.getElementById(players[i].bestCombination.cards[j].image).className = 'card';
		}
	}
};