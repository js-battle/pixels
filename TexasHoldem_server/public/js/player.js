﻿Player = function(name, bankroll) {
	var name = name || 'Player' + (Pixels.table.players.length + 1);

	this.cards = [];
	this.bet = 0;
	this.bankroll = bankroll || 200;
	this.fold = false;

	this.details = {
		bankrollUpGame: this.bankroll
	}

	this.getName = function() {
		return name;
	}

    //var field = document.createElement('div');
	var fieldForCards = document.createElement('div');
	var fieldForBet = document.createElement('strong');
	var fieldForBankroll = document.createElement('strong');

	fieldForCards.id = name;
	fieldForBet.id = 'bet' + name;
	fieldForBankroll.id = 'bankroll' + name;

    document.body.appendChild(fieldForBet);
    document.body.appendChild(fieldForCards);
    document.body.appendChild(fieldForBankroll);
};

Player.prototype.placeBet = function(value) {
	var bestBet = Pixels.table.getBestBet();
	var value = value > this.bankroll ? this.bankroll : value;

	this.bet += value;
	this.bankroll -= value;

	if (arguments.length == 1) {
		var operation = bestBet < Pixels.table.getBestBet() ? 'поднял' : this.bet == bestBet ?  'уровнял' :
			'поставил остатки';
		console.log(this.getName() + ':  ' + operation + ' с ' + (this.bet - value) + ' до ' + this.bet);
		Pixels.game.termsTrade(); // Продолжаем круг.
	}
};

Player.prototype.equalize = function() {
	var bestBet = Pixels.table.getBestBet();
	var value = bestBet > this.bet + this.bankroll ? this.bankroll : bestBet - this.bet;

	if (arguments.length) this.placeBet(value, false);
	else this.placeBet(value);
};

Player.prototype.raiseBet = function() {
	var newBet = +document.getElementById('setRaise').value;
	//newBet = newBet > Pixels.table.players[0].
	Pixels.table.players[0].placeBet(newBet);
};

Player.prototype.foldCards = function() {
	console.log(this.getName() + ' сбросил');
	this.fold = true;

	document.getElementById(this.getName()).style.display = 'none';

	var value = 0;
    var index = 0;
	for (var i = 0; i < Pixels.table.players.length; i++) {
		value += Pixels.table.players[i].fold ? 0 : 1;
        index = Pixels.table.players[i].fold ? index : i;
	}


	
	if (value == 1) {
        Pixels.table.distributionBet();
        Pixels.game.decidingWinner(index);
        Pixels.game.table.updateTable(true, false);

        var xhr = new XMLHttpRequest();
        xhr.open("GET", '/decidingWinner?identificator=' + Pixels.identificator, true);
        xhr.send();
    }
	else Pixels.game.termsTrade();
};

Player.prototype.determineMoveComputer = function() {
	//*******************************************************************Логика компов***********************************************************************************//
	var points = this.scanHand().value;
	var bestBet = Pixels.table.getBestBet();
	var difference = bestBet - this.bet;

	var value = getRandomMinMax(points * 30, this.bet - bestBet + 1000);

	if (value < 200) return {name: 'raiseBet', options: {newBet: this.bankroll * 0.6 ^ 0 + difference}};
	else if (value < 500) return {name: 'raiseBet', options: {newBet: (this.bankroll > 4 ? this.bankroll * 0.05 + 5 ^ 0 :
        this.bankroll) + difference}};
	else if (value > 900 && difference > this.bankroll * 0.4) return {name: 'fold'};
	else return {name: 'equalize'};
};

Player.prototype.scanHand = function() {

	var infoAboutCombination = {
		value: 0,
		cards: []
	};

	var cards = [];

	this.cards.sort(SortPropertyValue);

	for (var i = 0; i < this.cards.length; i++) {
		cards.push(this.cards[i]);
		//infoAboutCombination.value += (* - this.cards[i].value) / Math.pow(10, i + 2);
	}

	for (var i = 0; i < Pixels.table.cards.length; i++) {
		cards.push(Pixels.table.cards[i]);
	}

	cards.sort(SortPropertyValue);

	var bestLear = searchBestLear(cards);

	if (bestLear) {
		// Роял - флеш,  Стрит - флеш. (1 и 2-ое место.) и флеш (5 - ое место).

		var bestLearCards = [];

		for (var i = 0; i < cards.length; i++) {

			if (cards[i].lear == bestLear) {
				bestLearCards.push(cards[i]);
			}
		}
// Поиск последовательности в картах
		var sequenceCards = searchSequenceCards(bestLearCards);

		if (sequenceCards && sequenceCards[0].name == 'A') {
			infoAboutCombination.value = 1; // Роял - флеш
			infoAboutCombination.name = 'Royal Flush';
		}
		else if (sequenceCards) {
			infoAboutCombination.value = 2; // Стрит - флеш
			infoAboutCombination.name = 'Straight Flush';
			
		}
		else {
			infoAboutCombination.value = 5; // Флеш 
			infoAboutCombination.name = 'Flush';
		}

		infoAboutCombination.cards = sequenceCards ? sequenceCards : bestLearCards.slice(0, 5);
		infoAboutCombination.value = (15 - infoAboutCombination.cards[0].value) / 100 +
			(15 - infoAboutCombination.cards[3].value) / 1000 + infoAboutCombination.value;

		return infoAboutCombination;
	}

	var sequenceCards = searchSequenceCards(cards);

	if (sequenceCards) {
		infoAboutCombination.value = 6 + (15 - sequenceCards[0].value) / 100; // Стрит
		infoAboutCombination.cards = sequenceCards;
		infoAboutCombination.name = 'Straight';

		return infoAboutCombination;
	}

	// Ищем пары, тройки, каре.
	var numberRepetitions = [];

	for (var i = 0; i < cards.length; i++) {
		numberRepetitions[cards[i].name] = numberRepetitions[cards[i].name] === undefined ? 1 :
			numberRepetitions[cards[i].name] + 1;
	}

	var topNameCard = getBestNameCards(numberRepetitions);
	var bestCombinationCards = [];
// Начинаем создавать вывод лучшей комбинации для данного игрока на экран
	for (var i = 0; i < cards.length; i++) {

		if (cards[i].name == topNameCard) {
			infoAboutCombination.cards.push(cards[i]);
		}
	}

	if (numberRepetitions[topNameCard] == 4) {
		this.infoAboutCombination.value = 3 + (15 - infoAboutCombination.cards[0].value) / 100; // Каре
		infoAboutCombination.name = 'Four of a Kind';

		return infoAboutCombination;
	}
	
	var topNameCardValue = numberRepetitions[topNameCard];
	delete numberRepetitions[topNameCard];                 // Подумать, переделать.
	var topNameCard2 = getBestNameCards(numberRepetitions);
	
	if (numberRepetitions[topNameCard2] >= 2) {

		for (var i = 0; i < cards.length; i++) {

			if (cards[i].name == topNameCard2) {
				infoAboutCombination.cards.push(cards[i]);
			}
		}
	}

	if (topNameCardValue == 3) {

		if (numberRepetitions[topNameCard2] >= 2) {
			infoAboutCombination.value = 4 + (15 - infoAboutCombination.cards[0].value) / 100 + 
				(15 - infoAboutCombination.cards[3].value) / 1000; // Фулл - хаус
			infoAboutCombination.name = 'Full House';
		}
		else {
			infoAboutCombination.value = 7 + (15 - infoAboutCombination.cards[0].value) / 100; // Три карты одного номинала
			infoAboutCombination.name = 'Three of a kind';
		}

		return infoAboutCombination;
	}

	if (topNameCardValue == 2) {

		if (numberRepetitions[topNameCard2] == 2) {
			infoAboutCombination.value = 8 + (15 - infoAboutCombination.cards[0].value) / 100 +
			(15 - infoAboutCombination.cards[2].value) / 1000; // Две пары
				infoAboutCombination.name = 'Two Pair';
		}
		else {
			infoAboutCombination.value = 9 + (15 - infoAboutCombination.cards[0].value) / 100; // Пара
			infoAboutCombination.name = 'Pair';
		}
		
		
		return infoAboutCombination;
	}

	infoAboutCombination.value = 10 + (15 - this.cards[0].value) / 100 + (15 - this.cards[1].value) / 1000;
	infoAboutCombination.name = 'High Card';

	return infoAboutCombination;
};


function searchBestLear(cards) {

	var lears = [];

	for (var i = 0; i < cards.length; i++) {
		lears[cards[i].lear] = lears[cards[i].lear] === undefined ? 1 : lears[cards[i].lear] + 1;
	}

	var lear = getBestNameCards(lears);

	return lears[lear] > 4 ? lear : 0;
}

function getBestNameCards(cards) {

	var name; 
	var number = 0;

	for(var key in cards) {

		name = cards[key] > number ? key : name;
		number = cards[key] > number ? cards[key] : number;
	}

	return name;
}
// Поиск последовательности карт (например 5, 6, 7, 8, 9)
function searchSequenceCards(cards) {
	var newCards = [cards[0]];
	
	for (var i = 1; i < cards.length; i++) {

		if (newCards[newCards.length - 1].value == cards[i].value + 1) {
			newCards.push(cards[i]);
		}
		else if (newCards[newCards.length - 1].value != cards[i].value && newCards.length < 5) {
			newCards = [cards[i]];
		}
	}
	
	if (cards[0] && cards[0].name == 'A' && newCards[newCards.length - 1].name == 'two') {
		newCards.push(cards[0]);
	}
	
	return newCards.length > 4 ? newCards.slice(0, 5) : null;
}

function compareRandom(a, b) 
{
	return Math.random() - 0.5;
}

function SortPropertyValue(card1, card2)
{
	return card2.value - card1.value;
}

function sortByCombination(player1, player2)
{
	return player1.bestCombination.value - player2.bestCombination.value;
}

function getRandomIdentificator(roomId) {
    var identificator = 'player' + Object.keys(identificationCustomers).length;

    identificationCustomers[identificator] = roomId;

    return identificator;
}