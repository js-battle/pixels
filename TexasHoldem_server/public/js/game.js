﻿Game = function(table) {
	this.table = table;
	this.dispenser;
	this.whoWalks;
	this.numberMove = 0;
}

Game.prototype.startGame = function() {

    if (this.dispenser == undefined) {
        this.dispenser = this.table.players.length - 1;
        this.whoWalks = this.dispenser;
    }

	var table = Pixels.table;

	table.clearTable();
	table.updateTable();
// Определяются 2 игрока перед сдающим, и делаются ставки(до просмотра своих карт).
	var index = (table.players.length + Pixels.game.dispenser - 1) % table.players.length;

	table.players[index].placeBet(table.getBlind() / 2, false);
	table.players[Pixels.game.dispenser].placeBet(table.getBlind(), false);
	console.log(table.players[index].getName() + ' поставил пол-блайнда: ' + table.getBlind() / 2);
	console.log(table.players[Pixels.game.dispenser].getName() + ' поставил блайнд: ' + table.getBlind());

	Pixels.game.numberMove = 2;
}

Game.prototype.distributingCards = function() {

    document.getElementById('distributingCards').style.display = 'none';
    sendRequest('dropCard');
}

Game.prototype.termsTrade = function() {
	var bestBet = Pixels.game.table.getBestBet();

    Pixels.game.whoWalks = (Pixels.game.whoWalks + 1) % Pixels.game.table.players.length;
    Pixels.game.numberMove++;
    Pixels.game.table.updateTable();

	if (Pixels.game.numberMove > Pixels.game.table.players.length) {
		var index = Pixels.game.table.players.length - 1;

		while(index > -1 && (Pixels.game.table.players[index].bet == bestBet || !Pixels.game.table.players[index].bankroll ||
            Pixels.game.table.players[index].fold) ) {

			index--;
		}

		if (!~index) {
			console.log('Круг торговли закончен');
			console.log('');
            Pixels.game.numberMove = 0;
            Pixels.game.determineStageGame(); // Если у всех bet равняется максимальной ставки или банк пуст или чел пассанул, заканчиваем круг.
			return;
		}
	}
    // логика ботов
    if (Pixels.game.whoWalks > Pixels.table.playerHuman && Pixels.table.players[Pixels.game.whoWalks].cards.length &&
        Pixels.table.players[Pixels.game.whoWalks].bankroll && !Pixels.table.players[Pixels.game.whoWalks].fold) {

        var move = Pixels.table.players[Pixels.game.whoWalks].determineMoveComputer();
        var result = checkFinishRound(move.name);

        if (result) {
            setTimeout(function() {sendRequest(result.name, result.options) }, 500); //sendRequest(result.name);
        }
        else {
            setTimeout(function() {sendRequest(move.name, move.options)}, 500);
            if (move.name == 'equalize')
                Pixels.table.players[Pixels.game.whoWalks].equalize();
            else if (move.name == 'raiseBet')
                Pixels.table.players[Pixels.game.whoWalks].placeBet(move.options.newBet);
            else Pixels.table.players[Pixels.game.whoWalks].fold();
        }
        return;
    }

	if (Pixels.game.whoWalks == Pixels.playerIndex && !Pixels.game.table.players[Pixels.game.whoWalks].fold &&
        Pixels.game.table.players[Pixels.game.whoWalks].bankroll) {

		var callValue = Pixels.table.getBestBet() - +Pixels.game.table.players[Pixels.game.whoWalks].bet >
            Pixels.game.table.players[Pixels.game.whoWalks].bankroll ? Pixels.game.table.players[Pixels.game.whoWalks]
                .bankroll : Pixels.table.getBestBet() - +Pixels.game.table.players[Pixels.game.whoWalks].bet;

		document.getElementById('panel').style.display = 'inline';
		document.getElementById('equalize').innerHTML = !callValue ? 'check' : 'call  ' + callValue;
	}

    else if (Pixels.game.table.players[Pixels.game.whoWalks].fold || !Pixels.game.table.players[Pixels.game.whoWalks].bankroll) {
        for (var i  = 0; i < Pixels.table.players.length; i++) {
            if (Pixels.game.table.players[i].fold && !Pixels.game.table.players[i].bankroll) continue
            this.termsTrade();

            return;
        }
    }
}

Game.prototype.determineStageGame = function() {
	// После круга забираем у всех bet и вкладываем в общий pot.
	Pixels.table.distributionBet();
    Pixels.table.updateTable();

	if (this.table.cards.length < 5) {
		this.termsTrade(); // Начало круга
	}
	else {
        this.decidingWinner();
    }
}

Game.prototype.decidingWinner = function() {
    if (arguments.length) {
        for (var i = 0; i < Pixels.table.pot.length; i++)
            Pixels.table.players[+arguments[0]].bankroll += Pixels.table.pot[i];
    }
    else {
        var repositoryPlayers = this.table.players.slice();

        for (var i = 0; i < this.table.players.length; i++) {
            repositoryPlayers[i].id = i;
            repositoryPlayers[i].bestCombination = {};

            if (!this.table.players[i].numPots)
                this.table.players[i].numPots = this.table.pot.length;

            if (this.table.players[i].fold)
                repositoryPlayers[i].bestCombination.value = 15;
            else {
                repositoryPlayers[i].bestCombination = this.table.players[i].scanHand();
                var name = this.table.players[i].getName();
                var value = repositoryPlayers[i].bestCombination.name;

                document.getElementById('bet' + name).innerHTML = value;
            }
        }

        repositoryPlayers.sort(sortByCombination);

        Pixels.game.distributionChip(repositoryPlayers, 0);
    }

	document.getElementById('panel').style.display = 'none';
	
	setTimeout( function() {

		for (var i = 0; i < Pixels.table.players.length; i++) {

			if (!Pixels.table.players[i].bankroll) {
                if (Pixels.playerIndex > i) {
                    Pixels.playerIndex--;
                }
                else if (Pixels.playerIndex == i) {
                    console.log('Вы перешли в режим зрителя');
                    Pixels.playerIndex = -1;
                    sendRequest('removePlayer');
                }

				Pixels.table.removePlayer(i), i--;
			}
		}

		if (Pixels.table.players.length == 1) {
			console.log('С фишками отсался только: ' + Pixels.table.players[0].getName());
			return;
		}

		Pixels.table.pot = [0];
		Pixels.game.dispenser = (Pixels.game.dispenser + 1) % Pixels.table.players.length;
		Pixels.game.whoWalks = Pixels.game.dispenser;
        Pixels.game.table.updateTable(false);

        if ((Pixels.game.dispenser + 1) % Pixels.table.players.length > Pixels.table.playerHuman && !Pixels.playerIndex) {
            setTimeout(function(){sendRequest('dropCard')}, 500);
        }
        else if ((Pixels.game.dispenser + 1) % Pixels.table.players.length == Pixels.playerIndex) {
            document.getElementById('distributingCards').style.display = 'inline';
        }
	}, 5000);
}

Game.prototype.distributionChip = function(players, index) {
	var numWinners = [];

	var numberWinners = 1;
	while (numberWinners < players.length && players[0].bestCombination.value ==
		players[numberWinners].bestCombination.value) {
		numberWinners++;
	}

	for (var i = 0; i < numberWinners; i++) {
		
		for (var j = index; j < players[i].numPots; j++) {
			numWinners[j] = numWinners[j] != undefined ? numWinners[j] + 1 : 1;
		}
	}
	
	for (var i = 0; i < numberWinners; i++) {
		var jackpot = 0;
		
		for (var j = index; j < numWinners.length; j++) {
			jackpot += players[i].numPots > j ? this.table.pot[j] / numWinners[j] : 0;
		}

		console.log(this.table.players[players[i].id].getName() + ',  выигрыш составил: ' + (jackpot ^ 0)+ ', поставлено ' +
			'фишек в этой игре: ' + (this.table.players[players[i].id].details.bankrollUpGame - this.table.players
				[players[i].id].bankroll) );

		this.table.players[players[i].id].bankroll += jackpot ^ 0;
	}
	
	for (var j = index; j < numWinners.length; j++) 
		this.table.pot[j] = 0;

	if (players.length == this.table.players.length) 
		this.table.reduceSizeCards(players.slice(0, numberWinners));

	if (numWinners.length < this.table.pot.length) {
		this.distributionChip(players.slice(numberWinners), numWinners.length);
	}
	
	console.log('');
	console.log('');
}

function getRandomMinMax(min, max) { // Дря равномерного распределения вероятности каждого числа от min до max - 1.
    var rand = Math.random();

    return ~-rand ? min + (max - min) * rand ^ 0 : getRandomMinMax(min, max);
}