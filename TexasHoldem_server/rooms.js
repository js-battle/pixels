var log = require('libs/log')(module);

var visitors = []; // Люди на странице со списком комнат
var rooms = [];
var identificationCustomers = {}; // Чуваки за столами(у каждого идентификатора свой id стола).

exports.addVisitor = function(res) {
    visitors.push(res);

    res.on('close', function() {
        log.info('Удаляем визитора');
        visitors.splice(visitors.indexOf(res), 1);
    });
}

function publishEvent(options) {

    log.info('visitors LEngth:' + visitors.length);
    visitors.forEach(function(res) {
        res.end(JSON.stringify(options));
    });

    visitors = [];
}

exports.addRoom = function(res, options) {
    log.info('Параметры комнаты: ' + options.nameRoom + '/' + options.maxHuman);
    var room = new Rooms(options.nameRoom, options.maxHuman);
    // Информируем о появлении новой комнаты
    options.eventName = 'addRoom';
    options.roomId = rooms.length;

    publishEvent(options);

    rooms.push(room);
}

exports.getRooms = function() {
    return rooms;
}
// Заносит игрока в базу играющих и его номер стола и отправляем ему его идентификатор.
exports.addPlayer = function(res, roomId) {
    var room  = rooms[roomId];

    if (roomId < 0 || roomId > rooms.length - 1) {
        res.end('There is no room');
        return;
    }

    if (room.getPlayers() == room.getMaxHuman()) {
        res.end('Sorry, no place');
        return;
    }

    var identificator = getRandomIdentificator(roomId);

    room.addPlayer(identificator);
    room.eventOccurred(identificator, 'addPlayer'); // Сообщить остальным участникам комнаты, что зашел чел

    publishEvent({eventName: 'addPlayer', roomId: roomId}); // Информировать сидящих в списке комнат о занятом месте

    res.end(JSON.stringify({identificator: identificator, playerIndex: room.getPlayers() - 1,
        maxPlayer: room.getMaxHuman()}));
}

exports.removePlayer = function(res, options) {
    var idRoom = identificationCustomers[options.identificator];

    rooms[idRoom].removePlayer();
    rooms[idRoom].addViewer(options.identificator);

    res.end('You removed');
}
// Добавить слежение за обновлениями, и молниеносным ответом, в случае новых данных
exports.subscribe = function(res, identificator) {
    var idRoom = identificationCustomers[identificator];

    if (idRoom == undefined) {
        res.end('Вы ненаходитесь ни в одной из комнат');
        return;
    }

    rooms[idRoom].addFollowers(res, identificator);
}

exports.dropCard = function(res, options) {
    var idRoom = identificationCustomers[options.identificator];
    var cards = rooms[idRoom].cardsInGame;
    var deck = rooms[idRoom].deck;
    var numPlayer = rooms[idRoom].getMaxHuman();

    if (!cards.length) {

        for (var i = 0; i < 2 * numPlayer; i++) {
            cards.push(deck.pop());
        }
        log.info(cards);
        sendCards(idRoom, cards, res);
    }

    else if (cards.length == 2 * numPlayer) {

        for (var i = 0; i < 3; i++) {
            cards.push(deck.pop());
        }

        sendCards(idRoom, cards.slice(2 * numPlayer), res);
    }

    else {
        cards.push(deck.pop());
        sendCards(idRoom, cards.slice(-1), res);
    }
}

exports.eventOccurred = function(res, options, eventName) {
    var idRoom = identificationCustomers[options.identificator];

    rooms[idRoom].eventOccurred(options.identificator, eventName, options.newBet);
    res.end('Event occurred');
}

exports.decidingWinner = function(res, options) {
    var idRoom = identificationCustomers[options.identificator];

    // Случай когда, фишки в банкРолле остались у 0-1 людей
    if (options.noChips != undefined) {
        var cards = rooms[idRoom].cardsInGame;
        var allCardsNum = rooms[idRoom].getMaxHuman() * 2 + 5;
        var initialLength = cards.length > allCardsNum - 5 ? cards.length : 0;

        while(cards.length < allCardsNum) {
            cards.push(rooms[idRoom].deck.pop());
        }

        /*if (initialLength) {
            cards.splice(rooms[idRoom].getPlayers() * 2, initialLength);
        }*/

        var follower = rooms[idRoom].getFollowers();

        for (key in follower) {
            follower[key].end(JSON.stringify({cards: cards, comment: 'no chips', nameEvent: options.nameEvent}));
        }

        rooms[idRoom].refreshTable(); // Конец партии обновляем стол;
        res.end('ok');
        return;
    }

    // Случай, когда все сбросили кроме одного
    if (rooms[idRoom].cardsInGame.length < 2 * rooms[idRoom].getPlayers() + 5) {
        rooms[idRoom].refreshTable(); // Конец партии обновляем стол;
        res.end('ok');
        return
    }
    // Стандартный случай конца игры
    sendCards(idRoom, rooms[idRoom].cardsInGame.slice(0, 2 * rooms[idRoom].getMaxHuman()), res);
}

// Так же можно добавить такие параметры как, блайнд, мин ставка, макс ставка и многое другое
var Rooms = function(name, maxHuman) {
    this._name = name || 'No name ';
    this._maxHuman = maxHuman || 2;
    this._numPlayers = 0;
    this._followers = {};
    this._viewers = {};
    //this._followersViewer = {};

    this.cardsInGame = [];
    this.deck = new Deck();

    this.shuffleCards();
}

Rooms.prototype.eventOccurred = function(identificator, eventName, newBet) {
    var follower = this.getFollowers();

    for (key in follower) {
        if (key == identificator) continue
        follower[key].end(JSON.stringify({eventName: eventName, newBet: newBet}));
    }

    this.addFollowers(follower[identificator], identificator); // Тот кто вызвал событие остается в подписчиках
    log.info('Вернули в обработчик: ' + identificator);
};

Rooms.prototype.refreshTable = function() {
    this.deck = new Deck();
    this.cardsInGame = [];

    this.shuffleCards();
};

Rooms.prototype.addPlayer = function(player) {
    log.info('Зашел в игру чел')
    this._numPlayers++;
};

Rooms.prototype.removePlayer = function(player) {
    log.info('Перешел из стола в зрители')
    this._numPlayers--;
    this._maxHuman--;
};

Rooms.prototype.addViewer = function(identificator) {
    this._viewers[identificator] = true;
};

Rooms.prototype.getPlayers = function() {
    return this._numPlayers;
};

Rooms.prototype.checkViewers = function(identificator) {
    return this._viewers[identificator];
};

Rooms.prototype.addFollowers = function(res, identificator) {
    this._followers[identificator] = res;
};

Rooms.prototype.getFollowers = function() {
    var reserve = Object.assign({}, this._followers);

    this._followers = {};

    return reserve;
};

Rooms.prototype.getMaxHuman = function() {
    return this._maxHuman;
};

Rooms.prototype.shuffleCards =  function() {
    for (var i = 0; i < 10; i++) {
        this.deck.sort(compareRandom);
    }
};

Deck = function() {
    var fullDeck = [];

    for (var i = 0; i < 52; i++) {
        fullDeck[i] = {
            name: i % 13  == 0 ? 'A' : i % 13  == 1 ? 'two' : i % 13  == 2 ? 'three' : i % 13  == 3 ? 'four'
                : i % 13  == 4 ? 'five' : i % 13  == 5 ? 'six' : i % 13  == 6 ? 'seven' : i % 13  == 7 ? 'eight' : i % 13
                == 8 ? 'nine' : i % 13  == 9 ? 'ten' : i % 13  == 10 ? 'J' : i % 13  == 11 ? 'Q' : 'K',
            value: i % 13 ? (i % 13) + 1 : 14,
            lear: i < 13 ? 'diamonds' : i < 26 ? 'spades' : i < 39 ? 'clubs' : 'hearts',
            image: 'card' + (i + 1)
        };
    }
    return fullDeck;
};


//Временно пока development
function getRandomIdentificator(roomId) {
    var identificator = 'player' + Object.keys(identificationCustomers).length;

    identificationCustomers[identificator] = roomId;

    return identificator;
}

// Нужен апдейт подписки, на проверку при запросе новизны данных.
function sendCards(id, cards, res) {
    var followers = rooms[id].getFollowers();

    if (cards.length % 2) {// 1 или 3 карты, значит дроп карт на стол(отдаются карты всем игрокам).

        for (key in followers) {
            followers[key].end(JSON.stringify({cards: cards}));
        }
    }
    else if (cards.length == rooms[id].cardsInGame.length){// иначе четное, 2 * кол-во игроков, раздаем каждому по 2
        var index = 0;
        var cardsForBots = cards.slice(rooms[id].getPlayers() * 2);

        for (key in followers) {
            // Если нет в списке зрителей, получает карты
            if (rooms[id].checkViewers(key) == undefined) {
                if (!index)
                    followers[key].end(JSON.stringify({cards: cards.slice(index, index + 2).concat(cardsForBots),
                        comment: "firstCards"}));
                else
                    followers[key].end(JSON.stringify({cards: cards.slice(index, index + 2), comment: "firstCards"}));
                index += 2;
            }
            else {
                followers[key].end(JSON.stringify({cards: [], comment: "firstCards"}));
            }
        }
    }
    else {
        log.info(cards);

        for (key in followers) {
            log.info(key);
            followers[key].end(JSON.stringify({cards: cards}));
        }

        rooms[id].refreshTable(); // Конец партии обновляем стол
    }

    res.end('load cards');
}

function compareRandom(a, b)
{
    return Math.random() - 0.5;
}

setInterval(function() {
    log.info('-------------------------------------------------');
    log.info('visitors Length:' + visitors.length);
    for (var i = 0; i < rooms.length; i++) {
        log.info('Комната с id: ' + i + ', Кол-во людей: ' + Object.keys(rooms[i]._followers).length);
    }
}, 10000);