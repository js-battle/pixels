var express = require('express');
var http = require('http');
var path = require('path');
var config = require('config');
var log = require('libs/log')(module);

var app = express();

app.engine('ejs', require('ejs-locals'));
app.set('views', path.join(__dirname, 'templates'));
app.set('view engine', 'ejs');

app.use(express.favicon());
if (app.get('env') == 'development') {
    app.use(express.logger('dev'));
}
else {
    app.use(express.logger('default'));
}

app.use(express.json());
app.use(express.urlencoded());
app.use(app.router);

require('routes')(app);

app.use(express.static(path.join(__dirname, 'public')));

app.use(function(err, req, res, next) {
    log.info(app.get('env'));
    if (app.get('env') == 'development') {
        var errorHandler = express.errorHandler();
        errorHandler(err, req, res, next);
    }
    else {
        res.send(500);
    }
});

http.createServer(app).listen(config.get('port'), function() {
    log.info('Express server listening on port: ' + config.get('port'));
});

